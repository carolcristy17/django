# from django.db import models
# from datetime import date


# class Member(models.Model):
#   firstname = models.CharField(max_length=255)
#   lastname = models.CharField(max_length=255)
#   phone = models.IntegerField(null=True)
#   joined_date = models.DateField(null=True)
#   dob = models.DateField(null=True, blank=True) 
#   get_age= models.IntegerField(null=True)
 
#   def __str__(self):
#     return f"{self.firstname} {self.lastname}"

# class Meta:
#     ordering = ('lastname')

# @property
# def get_age(self):
#   if(self.dob != None):
#     get_age = date.today().year - self.dob.year / 365.25
#     return get_age

from datetime import date
from django.db import models

class Member(models.Model):
   
    dob = models.DateField(null=True, blank=True)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    phone = models.IntegerField(null=True)
    joined_date = models.DateField(null=True)
    age= models.DurationField(default=None)

    # DisplayFields = ['firstname', 'lastname', 'joining_date', 'dob', 'age']

    @property
    def age(self):
        nw = date.today()
        dob = self.dob
        if dob:
            return (
                # dob.year - nw.year - ((nw.month, nw.day) < (dob.month, dob.day))
                # date.today().year - self.dob.year / 365.25
                nw.year - dob.year - ((nw.month, nw.day) < (dob.month, dob.day))
            )